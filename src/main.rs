use std::time::{Duration, Instant};


const NUMTERMS: i32 = 1000000;


fn aproximaxion_sin_paralelo() {
    let mut pi: f64 = 0.0;
    for i in 0..NUMTERMS {
        let term = 1.0 / (2 * i + 1) as f64;
        if i % 2 == 0 {
            pi += term;
        } else {
            pi -= term;
        }
    }

    pi *= 4.0;

    println!("La aproximacion de PI utilizando la serie de Leibniz con {} termino es: {}", NUMTERMS, pi)
}


fn aproximacion_con_paralelo() {

}


fn main() {
    println!("Aproximacion: ");
    let start_time: Instant = Instant::now();
    aproximaxion_sin_paralelo();
    let end_time: Duration = start_time.elapsed();
    println!("Esto es lo que ha durado en ejecutarse: {}", end_time.as_micros());


}
